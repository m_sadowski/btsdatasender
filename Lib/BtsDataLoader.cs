﻿// <copyright file="BtsDataLoader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib
{
    using System.Collections.Generic;

    public class BtsDataLoader
    {
        // Load from csv and parse to list of objects
        private readonly CSVDataLoader csvLoader;

        public BtsDataLoader(CSVDataLoader loader) => this.csvLoader = loader;

        public List<BtsSearchPostData> Load(string lineNumbers)
        {
            List<BtsSearchPostData> btsData = new List<BtsSearchPostData>();
            List<List<string>> csvData = this.csvLoader.Load(lineNumbers);
            csvData.ForEach(x =>
            {
                string network = x[5];
                string eNodeBId = x[7];
                string band = x[6];
                string lac = x[11];
                string cellId1 = x[8];
                string cellId2 = x[9];
                string cellId3 = x[10];
                string voivodship = x[1];
                string city = x[2];
                string location = x[3];

                var builder = new BtsSearchPostData.Builder();
                var btsPostData = builder
                    .Initialize()
                    .Update()
                    .SetNetwork(network)
                    .SetLTEInfo(eNodeBId, band, lac)
                    .SetCellIds(new string[] { cellId1, cellId2, cellId3 })
                    .SetLocation(voivodship, city, location)
                    .SetContact("m.sadowski.dev@gmail.com")
                    .Build();

                btsData.Add(btsPostData);
            });

            return btsData;
        }
    }
}
