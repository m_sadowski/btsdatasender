﻿// <copyright file="BtsSearchLogger.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public class BtsSearchLogger
    {
        private string dir;

        public BtsSearchLogger(string dir)
        {
            this.dir = dir;
        }

        public void Log(BtsSearchPostData btsData, string response)
        {
            string fileName = $"{btsData.Network}_{btsData.ENodeBId}_{btsData.Band}_{DateTime.Now.ToString("dd-MM-yyyy_HHmm")}.txt";
            string filePath = Path.Combine(this.dir, fileName);
            string fileContent = $"{btsData.ToString()}\n\nResponse:\n{response}";

            File.WriteAllText(filePath, fileContent);
        }
    }
}
