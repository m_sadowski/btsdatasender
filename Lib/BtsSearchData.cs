﻿// <copyright file="BtsSearchData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib
{
    using System.ComponentModel.DataAnnotations;
    using BtsDataSender.Lib.Attributes;
    using BtsDataSender.Lib.Enums;
    using Newtonsoft.Json;

    public class BtsSearchData
    {
        [Required]
        [JsonProperty("submit")]
        public string Submit { get; protected set; }

        [Required]
        [JsonProperty("dotyczy")]
        public string Refers { get; protected set; }

        [Required]
        [AllowedValues(new string[] { "T-Mobile", "Orange", "Plusa", "Aero 2", "Play" })]
        [JsonProperty("siec")]
        public string Network { get; protected set; }

        [Required]
        [AllowedValues(new string[] { "GSM 900", "GSM 1800", "UMTS 2100", "UMTS 900", "LTE800", "LTE900", "LTE1800", "LTE2100", "LTE2600" })]
        [JsonProperty("pasmo")]
        public string Band { get; protected set; }

        [JsonProperty("carrier")]
        public string Frequency { get; protected set; }

        [JsonProperty("RNC")]
        public string RNC { get; protected set; }

        [Required]
        [JsonProperty("lac")]
        public string LAC { get; protected set; }

        [JsonProperty("sektor_1")]
        public string CellId1 { get; protected set; }

        [JsonProperty("sektor_2")]
        public string CellId2 { get; protected set; }

        [JsonProperty("sektor_3")]
        public string CellId3 { get; protected set; }

        [JsonProperty("sektor_4")]
        public string CellId4 { get; protected set; }

        [JsonProperty("sektor_5")]
        public string CellId5 { get; protected set; }

        [JsonProperty("sektor_6")]
        public string CellId6 { get; protected set; }

        [JsonProperty("sektor_7")]
        public string CellId7 { get; protected set; }

        [JsonProperty("sektor_8")]
        public string CellId8 { get; protected set; }

        [JsonProperty("sektor_9")]
        public string CellId9 { get; protected set; }

        [JsonProperty("sektor_0")]
        public string CellId0 { get; protected set; }

        [Required]
        [AllowedValues(new string[] { "Podlaskie [10]" })]
        [JsonProperty("wojewodztwo")]
        public string Voivodship { get; protected set; }

        [Required]
        [JsonProperty("miejscowosc")]
        public string City { get; protected set; }

        [Required]
        [JsonProperty("lokalizacja")]
        public string Location
        {
            get
            {
                string location = this.Address;
                if (!string.IsNullOrEmpty(this.ENodeBId))
                {
                    location += $"\n EnodeBId {this.ENodeBId}";
                }

                return location;
            }
        }

        [Required]
        [EmailAddress]
        [JsonProperty("adres_email")]
        public string Email { get; protected set; }

        [Required]
        [JsonProperty("rownanie")]
        public string Captcha { get; protected set; }

        [JsonIgnore]
        public string ENodeBId { get; set; }

        [JsonIgnore]
        public string Address { get; set; }

        [JsonIgnore]
        public BtsTechnology Technology
        {
            get; protected set;
        }

        public override string ToString()
        {
            return $@"
            Równanie:    {this.Captcha},    
            Dotyczy:     {this.Refers},
            Sieć:        {this.Network},
            Pasmo:       {this.Band},
            LAC:         {this.LAC},
            Sektor1:     {this.CellId1},
            Sektor2:     {this.CellId2},
            Sektor3:     {this.CellId3},
            Województwo: {this.Voivodship},
            Miejscowość: {this.City},
            Lokalizacja: {this.Location}
            ";
        }
    }
}
