﻿// <copyright file="DataSender.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    public class DataSender
    {
        private readonly string apiUrl;

        public DataSender(string url)
        {
            this.apiUrl = url;
        }

        public async Task<string> PostData(object content)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            var resultContent = await this.PostStreamAsync(content, cts.Token);
            var resultString = await resultContent.ReadAsStringAsync();
            return resultString;
        }

        private void SerializeJsonIntoStream(object value, Stream stream)
        {
            using (var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None })
            {
                var js = new JsonSerializer();
                js.Serialize(jtw, value);
                jtw.Flush();
            }
        }

        private HttpContent CreateHttpContent(object content)
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                var ms = new MemoryStream();
                this.SerializeJsonIntoStream(content, ms);
                ms.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(ms);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }

            return httpContent;
        }

        private async Task<HttpContent> PostStreamAsync(object content, CancellationToken cancellationToken)
        {
            HttpContent responseContent = null;
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, this.apiUrl))
            using (var httpContent = this.CreateHttpContent(content))
            {
                request.Content = httpContent;

                var response = await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                    .ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                responseContent = response.Content;
            }

            return responseContent;
        }
    }
}
