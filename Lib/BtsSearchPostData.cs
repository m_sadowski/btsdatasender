﻿// <copyright file="BtsSearchPostData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;
    using BtsDataSender.Lib.Enums;

    public class BtsSearchPostData : BtsSearchData
    {
        public BtsSearchPostData(Builder builder)
        {
            this.Submit = builder.Submit;
            this.Refers = builder.Refers;
            this.Captcha = builder.Captcha;

            this.Network = builder.Network;
            this.Technology = builder.Technology;
            this.Band = builder.Band;
            this.Frequency = builder.Frequency;
            this.RNC = builder.RNC;
            this.ENodeBId = builder.ENodeBId;
            this.LAC = builder.LAC;

            this.CellId1 = builder.CellId1;
            this.CellId2 = builder.CellId2;
            this.CellId3 = builder.CellId3;
            this.CellId4 = builder.CellId4;
            this.CellId5 = builder.CellId5;
            this.CellId6 = builder.CellId6;
            this.CellId7 = builder.CellId7;
            this.CellId8 = builder.CellId8;
            this.CellId9 = builder.CellId9;
            this.CellId0 = builder.CellId0;

            this.Voivodship = builder.Voivodship;
            this.City = builder.City;
            this.Address = builder.Address;
            this.Email = builder.Email;
        }

        public class Builder : BtsSearchData
        {
            public Builder()
            {
            }

            public Builder Initialize()
            {
                this.Submit = "ok";
                this.Captcha = "siedem";
                return this;
            }

            public Builder AddNew()
            {
                this.Refers = "nowy";
                return this;
            }

            public Builder Update()
            {
                this.Refers = "aktualizacja";
                return this;
            }

            public Builder SetNetwork(string network)
            {
                this.Network = network;
                return this;
            }

            public Builder SetGSMInfo(string band, string lac)
            {
                this.Technology = BtsTechnology.GSM;
                this.Band = band;
                this.LAC = lac;
                return this;
            }

            public Builder SetUMTSInfo(string band, string lac, string rnc, string frequency)
            {
                this.Technology = BtsTechnology.UMTS;
                this.Band = band;
                this.LAC = lac;
                this.RNC = rnc;
                this.Frequency = frequency;
                return this;
            }

            public Builder SetLTEInfo(string eNodeBId, string band, string lac)
            {
                this.Technology = BtsTechnology.LTE;
                this.ENodeBId = eNodeBId;
                this.Band = band;
                this.LAC = lac;
                return this;
            }

            public Builder SetCellIds(string[] arr)
            {
                Type builderType = typeof(Builder);
                for (int i = 0; i < arr.Length; i++)
                {
                    PropertyInfo cellIdProperty = builderType.GetProperty($"CellId{i + 1}");
                    cellIdProperty.SetValue(this, arr[i]);
                }

                return this;
            }

            public Builder SetLocation(string voivodship, string city, string location)
            {
                this.Voivodship = voivodship;
                this.City = city;
                this.Address = location;
                return this;
            }

            public Builder SetContact(string email)
            {
                this.Email = email;
                return this;
            }

            public BtsSearchPostData Build(bool validate = true)
            {
                var btsSearchPostData = new BtsSearchPostData(this);
                if (validate)
                {
                    this.Validate(btsSearchPostData);
                }

                return btsSearchPostData;
            }

            private BtsSearchPostData Validate(BtsSearchPostData btsSearchPostData)
            {
                var context = new ValidationContext(btsSearchPostData, serviceProvider: null, items: null);
                var validationResults = new List<ValidationResult>();

                bool isValid = Validator.TryValidateObject(btsSearchPostData, context, validationResults, true);

                if (isValid)
                {
                    return btsSearchPostData;
                }

                throw new ValidationException(validationResults[0].ErrorMessage);
            }
        }
    }
}
