﻿// <copyright file="AllowedValuesAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib.Attributes
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;

    public class AllowedValuesAttribute : ValidationAttribute
    {
        public AllowedValuesAttribute(string[] values)
        {
            this.Values = values;
        }

        public string[] Values { get; }

        public string GetErrorMessage(string member) => $"{member} not valid.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Type btsSearchDataType = typeof(BtsSearchData);
            PropertyInfo validatedProperty = btsSearchDataType.GetProperty(validationContext.MemberName);
            string propValue = (string)validatedProperty.GetValue(validationContext.ObjectInstance);

            if (!this.Values.Contains(propValue))
            {
                return new ValidationResult(this.GetErrorMessage(validationContext.MemberName));
            }

            return ValidationResult.Success;
        }
    }
}
