﻿// <copyright file="BtsTechnology.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib.Enums
{
    public enum BtsTechnology
    {
        GSM,
        UMTS,
        LTE,
    }
}
