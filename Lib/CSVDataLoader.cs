﻿// <copyright file="CSVDataLoader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender.Lib
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class CSVDataLoader
    {
        private readonly string path = string.Empty;
        private readonly string delimiter;

        public CSVDataLoader(string path, string delimiter = ";")
        {
            this.path = path;
            this.delimiter = delimiter;
        }

        public List<List<string>> Load(string lineNumbers)
        {
            // lines moze byc po przecinku (1,2,3) lub zakres (2-5)
            List<List<string>> loadedData = new List<List<string>>();
            List<int> parsedLineNumbers = this.ParseLineNumbers(lineNumbers);
            int lineNumber = 0;

            using (var reader = new StreamReader(this.path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (parsedLineNumbers.Contains(lineNumber))
                    {
                        loadedData.Add(line.Split(',').ToList());
                    }

                    lineNumber++;
                }
            }

            return loadedData;
        }

        private List<int> ParseLineNumbers(string lineNumbers)
        {
            List<int> parsedLineNumbers = new List<int>();
            if (lineNumbers.Contains("-"))
            {
                var splitted = lineNumbers.Split("-", StringSplitOptions.RemoveEmptyEntries);
                var firstLine = int.Parse(splitted[0]);
                var lastLine = int.Parse(splitted[1]);
                for (var i = firstLine - 1; i <= lastLine - 1; i++)
                {
                    parsedLineNumbers.Add(i);
                }
            }
            else
            {
                var splitted = lineNumbers.Split(",", StringSplitOptions.RemoveEmptyEntries);
                parsedLineNumbers.AddRange(splitted.Select(x => int.Parse(x) - 1));
            }

            return parsedLineNumbers;
        }
    }
}
