﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.Attributes.AllowedValuesAttribute")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.Enums.BtsTechnology")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:Enumeration items should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.Enums.BtsTechnology")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.BtsDataLoader")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.BtsSearchData")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.BtsSearchPostData")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.CSVDataLoader")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.DataSender")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Program")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "a", Scope = "type", Target = "~T:BtsDataSender.Lib.BtsSearchLogger")]