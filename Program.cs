﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BtsDataSender
{
    using System;
    using System.Threading;
    using BtsDataSender.Lib;

    internal class Program
    {
        private static void Main(string[] args)
        {
            string url = "https://n5hfabocll.execute-api.eu-central-1.amazonaws.com/dev/btssearch";

            DataSender sender = new DataSender(url);
            CancellationTokenSource cts = new CancellationTokenSource();

            CSVDataLoader dataLoader = new CSVDataLoader("..\\..\\..\\inputData.csv");
            BtsDataLoader btsDataLoader = new BtsDataLoader(dataLoader);
            BtsSearchLogger btsSearchLogger = new BtsSearchLogger("..\\..\\..\\Logs\\");

            // var dataLoaded = btsDataLoader.Load("15-18");
            var dataLoaded = btsDataLoader.Load("15");

            Console.WriteLine("Data to send: \n");

            foreach (var data in dataLoaded)
            {
                Console.WriteLine(data.ToString());
                Console.WriteLine("Press ENTER to confirm");
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Enter)
                {
                    string result = sender.PostData(data).Result;
                    btsSearchLogger.Log(data, result);

                    Console.Write("Data has been sent successfully\n\n");
                }
            }

            Console.WriteLine("\n\nFINISHED !");
        }
    }
}
